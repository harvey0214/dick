// swift-tools-version:4.0
import PackageDescription


let package = Package(
    name: "Hello",
    products: [
        .library(name: "Hello", targets: ["App"]),
    ],
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.3.1"),
        // 🔵 Swift ORM (queries, models, relations, etc) built on SQLite 3.
        .package(url: "https://github.com/vapor/fluent-sqlite.git", from: "3.0.0"),
        // 🐘 Swift ORM (queries, models, relations, etc) built on PostgreSQL.
        .package(url: "https://github.com/vapor/fluent-postgresql.git", from: "1.0.0"),
        // ⚙️ A collection of Swift extensions for wide range of Vapor data types and classes.
        .package(url: "https://github.com/vapor-community/vapor-ext.git", from: "0.3.4"),
        // auth
        .package(url: "https://github.com/vapor/auth.git", from: "2.0.4")

    ],
    targets: [
        .target(name: "App", dependencies: ["FluentSQLite", "Vapor", "FluentPostgreSQL", "VaporExt", "Authentication"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

