import Vapor

public func setupCacheSessions(services: inout Services) throws {
    
    services.register(KeyedCache.self) { container in
        try container.keyedCache(for: .psql)
    }
    
    let sessionConfig = SessionsConfig(cookieName: "Hello") { (value) in
        return HTTPCookieValue(string: value, expires: Date(
            timeIntervalSinceNow: 60 * 60 * 24 * 1 // one day //not working for session cache
        ), maxAge: nil
            , domain: "localhost"
            , path: "/"
            , isSecure: true
            , isHTTPOnly: false
            , sameSite: nil)
        
    }
    
    services.register(sessionConfig)
}
