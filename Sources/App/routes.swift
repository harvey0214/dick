import Vapor


/// Register your application's routes here.
public func routes(_ router: Router) throws {
//
//    func getUser(_ req: Request) throws -> UserInfo {
//        let form = try req.query.decode(UserInfoRequest.self)
//        let name = form.name ?? ""
//        let age = form.age ?? 0
//        return UserInfo.create(name: name, age: age, gender: "make")
//    }
    // Basic "It works" example
    router.get { req in
        return "It works!"
    }
    
    try router.register(collection: UserInfoController())
    try router.register(collection: ClassController())
    try router.register(collection: StudentClassController())
    try router.register(collection: EmployeeController())
    try router.register(collection: CompanyController())
    try router.register(collection: CompanyEmployeeController())
    try router.register(collection: CompanyLocationController())
//    router.grouped("myUserInfo")
    // Basic "Hello, world!" example
//    router.get(use: getUser)
    
    
    // Example of configuring a controller
    let todoController = TodoController()
    router.get("todos", use: todoController.index)
    router.post("todos", use: todoController.create)
    router.delete("todos", Todo.parameter, use: todoController.delete)
    
}

struct UserInfoRequest: Content {
    let name: String?
    let age: Int?
}




