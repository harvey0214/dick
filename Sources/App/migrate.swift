import FluentPostgreSQL

public func migrate(config: inout MigrationConfig) {
    config.add(model: Todo.self, database: .psql)
    config.add(model: UserInfo.self, database: .psql)
    config.add(model: Classroom.self, database: .psql)
    config.add(model: StudentClass.self, database: .psql)
    config.add(model: Employee.self, database: .psql)
    config.add(model: Company.self, database: .psql)
    config.add(model: CompanyEmployee.self, database: .psql)
    config.add(model: CompanyLocation.self, database: .psql)
    config.prepareCache(for: .psql)
}
