import Vapor
import FluentPostgreSQL
import Authentication

final class Company: Decodable {
    var companyName: String
    var age: Int
    var majorOfProduct: String
    
    var id: Int?
    
    init(id: Int? = nil, companyName: String, age: Int, majorOfProduct: String) {
        self.companyName = companyName
        self.age = age
        self.majorOfProduct = majorOfProduct
    }
}


extension Company: Migration {}
extension Company: Parameter {}
extension Company: PostgreSQLModel {
    static var entity: String = "Companies"
}
extension Company: Content {}
extension Company: SessionAuthenticatable {}


extension Company {
    var Employees: Siblings<Company, Employee, CompanyEmployee> {
        return siblings()
    }
    
    var locations: Children<Company, CompanyLocation> {
        return children(\.company_id)
    }
}
