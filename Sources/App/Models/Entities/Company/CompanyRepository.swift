import VaporExt

protocol CompanyRepository: Service {
    func create(_ model: Company, on conn: DatabaseConnectable) -> Future<Company>
    func delete(_ model: Company, on conn: DatabaseConnectable) -> Future<Void>
    func update(_ model: Company, on conn: DatabaseConnectable) -> Future<Company>
    func fetchAll(on conn: DatabaseConnectable) -> Future<[Company]>
    func find(by criteria: [FilterOperator<Company.Database, Company>], sortBy: [Company.Database.QuerySort]?, range: (lower: Int, upper: Int), keyword: String?, on conn: DatabaseConnectable, withSoftDelete: Bool) -> Future<([Company], Int)>
    func findLocations(_ model: Company, by criteria: [FilterOperator<CompanyLocation.Database, CompanyLocation>], sortBy: [CompanyLocation.Database.QuerySort]?, range: (lower: Int, upper: Int), on conn: DatabaseConnectable, withSoftDeleted: Bool) throws -> Future<([CompanyLocation], Int)>
}

final class SQLCompanyRepository: CompanyRepository {
    func create(_ model: Company, on conn: DatabaseConnectable) -> EventLoopFuture<Company> {
        return model.create(on: conn)
    }
    
    func delete(_ model: Company, on conn: DatabaseConnectable) -> EventLoopFuture<Void> {
        return model.delete(on: conn)
    }
    
    func update(_ model: Company, on conn: DatabaseConnectable) -> EventLoopFuture<Company> {
        return model.update(on: conn)
    }
    
    func fetchAll(on conn: DatabaseConnectable) -> EventLoopFuture<[Company]> {
        return Company.query(on: conn).all()
    }
    
    func find(by criteria: [FilterOperator<Company.Database, Company>], sortBy: [Company.Database.QuerySort]?, range: (lower: Int, upper: Int), keyword: String?, on conn: DatabaseConnectable, withSoftDelete: Bool) -> EventLoopFuture<([Company], Int)> {
        let queryBuilder = Company.query(by: criteria, on: conn, withSoftDeleted: withSoftDelete).filter(\.companyName, .like, "%\(keyword ?? "")%")
        
        for filter in criteria {
            queryBuilder.filter(filter)
        }
        
        return queryBuilder.count().flatMap { (total) in
            return queryBuilder
                .sort(by: sortBy)
                .range(lower: range.lower, upper: range.upper)
                .all()
                .map { (companies) in
                    return (companies, total)
            }
        }
    }
    
    
    func findLocations(_ model: Company, by criteria: [FilterOperator<CompanyLocation.Database, CompanyLocation>], sortBy: [CompanyLocation.Database.QuerySort]?, range: (lower: Int, upper: Int), on conn: DatabaseConnectable, withSoftDeleted: Bool) throws -> Future<([CompanyLocation], Int)> {
        let queryBuilder = try model.locations.query(on: conn)
        
        for filter in criteria {
            queryBuilder.filter(filter)
        }
        
        return queryBuilder.count().flatMap { (total) in
            queryBuilder
                .sort(by: sortBy)
                .range(lower: range.lower, upper: range.upper)
                .all()
                .map { (locations) in
                    return (locations, total)
            }
        }
    }
    
}
