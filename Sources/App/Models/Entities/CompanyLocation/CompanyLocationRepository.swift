import VaporExt

protocol CompanyLocationRepository: Service {
    func create(_ model: CompanyLocation, on conn: DatabaseConnectable) -> Future<CompanyLocation>
    
}


final class SQLCompanyLocationRepository: CompanyLocationRepository {
    func create(_ model: CompanyLocation, on conn: DatabaseConnectable) -> Future<CompanyLocation> {
        return model.create(on: conn)
    }
    
    
}
