import Vapor
import FluentPostgreSQL

final class CompanyLocation: Decodable {
    
    var id: Int?
    var company_id: Company.ID
    var location: String
    
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    init(id: Int? = nil, company_id: Company.ID, location: String) {
        self.company_id = company_id
        self.location = location
    }
    
}

extension CompanyLocation: Migration {}
extension CompanyLocation: Content {}
extension CompanyLocation: PostgreSQLModel {
    static var entity: String = "CompanyLocation"
    
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

extension CompanyLocation {
    var company: Parent<CompanyLocation, Company>? {
        return parent(\.company_id)
    }
}

