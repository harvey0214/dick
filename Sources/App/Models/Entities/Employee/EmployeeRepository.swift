import VaporExt
import Fluent

protocol EmployeeRepository: Service {
    func create(_ model: Employee, on conn: DatabaseConnectable) -> Future<Employee>
    func delete(_ model: Employee, on conn: DatabaseConnectable) -> Future<Void>
    func update(_ model: Employee, on conn: DatabaseConnectable) -> Future<Employee>
    
    func fetchAll(on conn: DatabaseConnectable) -> Future<[Employee]>
    func find(by criteria: [FilterOperator<Employee.Database, Employee>], sortBy: [Employee.Database.QuerySort]?, range: (lower: Int, upper: Int), keyword: String?, on conn: DatabaseConnectable, withSoftDelete: Bool) -> Future<([Employee], Int)>
}

final class SQLEmployeeRepository: EmployeeRepository {
    func create(_ model: Employee, on conn: DatabaseConnectable) -> EventLoopFuture<Employee> {
        return model.create(on: conn)
    }
    
    func delete(_ model: Employee, on conn: DatabaseConnectable) -> Future<Void> {
        return model.delete(on: conn)
    }
    func update(_ model: Employee, on conn: DatabaseConnectable) -> Future<Employee> {
        return model.update(on: conn)
    }
    
    func fetchAll(on conn: DatabaseConnectable) -> EventLoopFuture<[Employee]> {
        return Employee.query(on: conn).all()
    }
    func find(by criteria: [FilterOperator<Employee.Database, Employee>], sortBy: [Employee.Database.QuerySort]?, range: (lower: Int, upper: Int), keyword: String?, on conn: DatabaseConnectable, withSoftDelete: Bool) -> Future<([Employee], Int)> {
        let queryBuilder = Employee.query(by: criteria, on: conn, withSoftDeleted: withSoftDelete).filter(\Employee.name, .like, "%\(keyword ?? "")%")
        
        return queryBuilder.count().flatMap { (total) in
            queryBuilder
                .sort(by: sortBy)
                .range(lower: range.lower, upper: range.upper)
                .all().map { (employees) in
                    return (employees, total)
            }
        }
    }
    
    
    
}

