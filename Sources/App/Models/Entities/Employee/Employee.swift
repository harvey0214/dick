import FluentPostgreSQL
import Vapor
import Authentication

final class Employee: Decodable {
    var id: Int?
    var name: String?
    var age: Int?
    var workYear: Int?
//    var company_id: Int?
    
    init(id: Int? = nil, name: String, age: Int, workYear: Int) {
        self.name = name
        self.age = age
        self.workYear = workYear
//        self.company_id = company_id
    }
}


extension Employee: Migration {}
extension Employee: Parameter {}
extension Employee: Content {}
extension Employee: PostgreSQLModel {
    static var entity: String = "Employees"
}
extension Employee: SessionAuthenticatable {}


extension Employee {
    var companies: Siblings<Employee, Company, CompanyEmployee> {
        return siblings()
    }
}
