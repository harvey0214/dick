import Vapor
import FluentPostgreSQL


final class UserInfo: Codable {
    var id: Int?
    var name: String?
    var age: Int?
    var gender: String?
    var classroom_id: Classroom.ID
    
    init(id: Int? = nil, name: String, age: Int, gender: String, classroom_id: Classroom.ID) {
        self.id = id
        self.name = name
        self.age = age
        self.gender = gender
        self.classroom_id = classroom_id
    }
}

extension UserInfo: PostgreSQLModel {
    static var entity = "UserInfo"
}

extension UserInfo: Migration {}

extension UserInfo: Content {}

/// Allows `Actor` to be used as a dynamic parameter in route definitions.
extension UserInfo: Parameter {}

extension UserInfo {
    var inClass: Parent<UserInfo, Classroom> {
        return parent(\.classroom_id)
    }
    
    var classes: Siblings<UserInfo, Classroom, StudentClass> {
        return siblings()
    }
}
