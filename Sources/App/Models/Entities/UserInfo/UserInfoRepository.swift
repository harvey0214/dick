import Fluent
import VaporExt

protocol UserInfoRepository: Service {
    func find(by criteria: [FilterOperator<UserInfo.Database, UserInfo>], sortBy: [UserInfo.Database.QuerySort]?, range: (lower: Int, upper: Int), gender: String?, age: Int?, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<([UserInfo], Int)>
    func fetchAll(on connectable: DatabaseConnectable) -> Future<[UserInfo]>
    func create(_ model: UserInfo, on connectable: DatabaseConnectable) -> Future<UserInfo>
    func findClass(by criteria: [FilterOperator<UserInfo.Database, UserInfo>], sortBy: [UserInfo.Database.QuerySort]?, range: (lower: Int, upper: Int), gender: String?, age: Int?, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<[Classroom]>
}


final class SQLUserInfoRepository: UserInfoRepository {
    func find(by criteria: [FilterOperator<UserInfo.Database, UserInfo>], sortBy: [UserInfo.Database.QuerySort]?, range: (lower: Int, upper: Int), gender: String?, age: Int?, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<([UserInfo], Int)> {
        let queryBuilder = UserInfo.query(by: criteria, on: connectable, withSoftDeleted: withSoftDeleted).filter(\UserInfo.gender, .like, "%\(gender ?? "")%")
        
        return queryBuilder.count().flatMap { (total)  in
            return queryBuilder.sort(by: sortBy).range(lower: range.lower, upper: range.upper).all().map { (userInfo) in
                return (userInfo, total)
            }
        }
    }
    
    func create(_ model: UserInfo, on connectable: DatabaseConnectable) -> Future<UserInfo> {
        return model.create(on: connectable)
    }
    
    func fetchAll(on connectable: DatabaseConnectable) -> Future<[UserInfo]> {
        let queryBuilder = UserInfo.query(on: connectable, withSoftDeleted: false)
        
        return queryBuilder.count().flatMap { (total) in
            return queryBuilder.all()
        }
    }
    
    func findClass(by criteria: [FilterOperator<UserInfo.Database, UserInfo>], sortBy: [UserInfo.Database.QuerySort]?, range: (lower: Int, upper: Int), gender: String?, age: Int?, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<[Classroom]> {
        
        let queryBuilder = UserInfo.query(by: criteria, on: connectable, withSoftDeleted: withSoftDeleted).filter(\UserInfo.gender, .like, "%\(gender ?? "")%")
        
        return queryBuilder.sort(by: sortBy).range(lower: range.lower, upper: range.upper).all().flatMap { (userInfo) in
            return userInfo[0].inClass.query(on: connectable).all()
        }
      
    }
}
    
