import Fluent
import VaporExt

protocol StudentClassRepository: Service {
    func attach(classroom: Classroom, student: UserInfo, on connectable: DatabaseConnectable) throws -> Future<StudentClass>
    func detach(classroom: Classroom, student: UserInfo, on connectable: DatabaseConnectable) throws -> Future<Void>
    func findOne(by criteria: [FilterOperator<StudentClass.Database, StudentClass>], sortBy: [StudentClass.Database.QuerySort]?, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<StudentClass?>
    func findUserInfos(by classroom: Classroom, criteria: [FilterOperator<StudentClass.Database, StudentClass>], sortBy: [StudentClass.Database.QuerySort]?, on connectable: DatabaseConnectable, withSoftDeleted: Bool) throws -> Future<[UserInfo]>
    func findClassroom(by student: UserInfo, criteria: [FilterOperator<StudentClass.Database, StudentClass>], sortBy: [StudentClass.Database.QuerySort]?, on conn: DatabaseConnectable, withSoftDeleted: Bool) throws -> Future<[Classroom]>
}


final class SQLStudenetClassRepository: StudentClassRepository {
    func attach(classroom: Classroom, student: UserInfo, on connectable: DatabaseConnectable) throws -> EventLoopFuture<StudentClass> {
        let studentClass = try StudentClass(classroom, student)
        return studentClass.save(on: connectable)
    }
    
    func detach(classroom: Classroom, student: UserInfo, on connectable: DatabaseConnectable) throws -> EventLoopFuture<Void> {
        return classroom.students.detach(student, on: connectable)
    }
    
    func findOne(by criteria: [FilterOperator<StudentClass.Database, StudentClass>], sortBy: [StudentClass.Database.QuerySort]?, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> EventLoopFuture<StudentClass?> {
        return StudentClass.findOne(by: criteria, on: connectable, withSoftDeleted: withSoftDeleted)
    }
    func findUserInfos(by classroom: Classroom, criteria: [FilterOperator<StudentClass.Database, StudentClass>], sortBy: [StudentClass.Database.QuerySort]?, on connectable: DatabaseConnectable, withSoftDeleted: Bool) throws -> Future<[UserInfo]> {
        let queryBuilder = try classroom.students.query(on: connectable)
        return queryBuilder.all()
    }
    
    func findClassroom(by student: UserInfo, criteria: [FilterOperator<StudentClass.Database, StudentClass>], sortBy: [StudentClass.Database.QuerySort]?, on conn: DatabaseConnectable, withSoftDeleted: Bool) throws -> Future<[Classroom]> {
        return try student.classes.query(on: conn).all()
    }


}
