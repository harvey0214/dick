import FluentPostgreSQL
import Vapor

final class StudentClass: Codable {
    var id: Int?
    
    var class_id: Classroom.ID
    var student_id: UserInfo.ID
    
    init(id: Int? = nil, student_id: UserInfo.ID, class_id: Classroom.ID) {
        self.student_id = student_id
        self.class_id = class_id
    }
    
    init(_ left: Classroom, _ right: UserInfo) throws {
        self.class_id = try left.requireID()
        self.student_id = try right.requireID()
    }

}

extension StudentClass: PostgreSQLPivot {
    static var entity: String = "class_student"
    typealias Left = Classroom
    typealias Right = UserInfo
    
    static var leftIDKey: WritableKeyPath<StudentClass, Int> = \.class_id
    static var rightIDKey: WritableKeyPath<StudentClass, Int> = \.student_id
}

extension StudentClass: Migration {}
extension StudentClass: Content {}
extension StudentClass: Parameter {}
extension StudentClass: ModifiablePivot {}



