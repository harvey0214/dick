import VaporExt
import Fluent


protocol ClassroomRepository: Service {
    func create(_ model: Classroom, on connectable: DatabaseConnectable) -> Future<Classroom>
    func findAll(on connectable: DatabaseConnectable) -> Future<[Classroom]>
    func findClassroomUserInfos(by criteria: [FilterOperator<Classroom.Database, Classroom>], sortBy: [Classroom.Database.QuerySort]?, index: Int, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<[UserInfo]>
    func findClassroom(by criteria: [FilterOperator<Classroom.Database, Classroom>], sortBy: [Classroom.Database.QuerySort]?, index: Int, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<Classroom>
}

final class SQLClassRepository: ClassroomRepository {
    func create(_ model: Classroom, on connectable: DatabaseConnectable) -> EventLoopFuture<Classroom> {
       return model.create(on: connectable)
    }
    
    func findAll(on connectable: DatabaseConnectable) -> EventLoopFuture<[Classroom]> {
        let queryBuilder = Classroom.query(on: connectable, withSoftDeleted: false)
        return queryBuilder.count().flatMap { (Int) in
            return queryBuilder.all()
        }
    }
    
    func findClassroomUserInfos(by criteria: [FilterOperator<Classroom.Database, Classroom>], sortBy: [Classroom.Database.QuerySort]?, index: Int, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<[UserInfo]> {
        let queryBuilder = Classroom.query(by: criteria, on: connectable, withSoftDeleted: withSoftDeleted)
        
        return queryBuilder.sort(by: sortBy).range(lower: index, upper: index).all().flatMap { (classes) in
            return try classes[0].userInfos.query(on: connectable).all()
        }
    }
    
    func findClassroom(by criteria: [FilterOperator<Classroom.Database, Classroom>], sortBy: [Classroom.Database.QuerySort]?, index: Int, on connectable: DatabaseConnectable, withSoftDeleted: Bool) -> Future<Classroom> {
        let queryBuilder = Classroom.query(by: criteria, on: connectable, withSoftDeleted: withSoftDeleted)
        return queryBuilder.sort(by: sortBy).range(lower: index, upper: index).all().map { (classes) in
            return classes[0]
        }
    }
}
