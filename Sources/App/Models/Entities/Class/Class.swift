import FluentPostgreSQL
import Vapor
import Authentication


final class Classroom :Codable {
    var id: Int?
    var className: String?
    var peopleCount: Int?
    var grade: Float?
    var teacher: String?
    
    init(id: Int? = nil, className: String, peopleCount: Int, grade: Float, teacher: String) {
        self.id = id
        self.className = className
        self.peopleCount = peopleCount
        self.grade = grade
        self.teacher = teacher
    }

}

extension Classroom: Migration {}

extension Classroom: PostgreSQLModel {
    static var entity: String = "Classes"
}

extension Classroom: Content {}
extension Classroom: Parameter {}
extension Classroom: SessionAuthenticatable {}
 
extension Classroom {
    var userInfos: Children<Classroom, UserInfo> {
        return children(\.classroom_id)
    }
    
    var students: Siblings<Classroom, UserInfo, StudentClass> {
        return siblings()
    }
}



struct AddClassTeachers: Migration {
    typealias Database = PostgreSQLDatabase
    
    static func prepare(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
//        return conn.future()
        return Classroom.Database.update(Classroom.self, on: conn) { (builder) in
            let defaultValue = PostgreSQLColumnConstraint.default(.literal(._string("Input Here")))

            builder.field(for: \.teacher, type: .text, defaultValue)
        }
    }
    
    static func revert(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
        return Classroom.revert(on: conn)
    }
    
    
}

