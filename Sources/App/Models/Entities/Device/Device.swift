import Vapor
import FluentPostgreSQL
import Authentication


final class Device: Decodable {
    var id: Int?
    var token: String
    var catalog: String
    
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    init(id: Int? = nil, token: String, catalog: String) {
        self.token = token
        self.catalog = catalog
    }
}

extension Device: Content {}
extension Device: Migration {}
extension Device: PostgreSQLModel {
    static var entity: String = "devices"
    
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

extension Device: Parameter {}

extension Device: Validatable {
    static func validations() throws -> Validations<Device> {
        var validations = Validations(Device.self)
        try validations.add(\.token, !.empty)
        try validations.add(\.catalog, !.empty)
        return validations
    }
}



