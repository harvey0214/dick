import Vapor
import FluentPostgreSQL

final class CompanyEmployee: Codable {
    var id: Int?
    
    var company_id: Company.ID
    var employee_id: Employee.ID
    
    var createAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    init(id: Int? = nil, company_id: Company.ID, employee_id: Employee.ID) {
        self.id = id
        self.company_id = company_id
        self.employee_id = employee_id
    }
    
    init(_ left: Company, _ right: Employee) throws {
        self.company_id = try left.requireID()
        self.employee_id = try right.requireID()
    }
    
}

extension CompanyEmployee: PostgreSQLPivot {
    static var entity: String = "Company_Employee"
    typealias Left = Company
    typealias Right = Employee

    static var leftIDKey: LeftIDKey = \.company_id
    static var rightIDKey: RightIDKey = \.employee_id
}

extension CompanyEmployee: ModifiablePivot {}
extension CompanyEmployee: Migration {}
extension CompanyEmployee: Content {}
extension CompanyEmployee: Parameter {}
