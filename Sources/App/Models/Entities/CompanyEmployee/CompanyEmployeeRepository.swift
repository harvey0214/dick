import Fluent
import VaporExt

protocol CompanyEmployeeRepository: Service {
    func findEmployees(by company: Company, criteria: [FilterOperator<Employee.Database, Employee>], sortBy: [Employee.Database.QuerySort]?, range: (lower: Int, upper: Int), on conn: DatabaseConnectable) throws -> Future<([Employee], Int)>
    func findCompany(by employee: Employee, criteria: [FilterOperator<Company.Database, Company>], sortBy: [Company.Database.QuerySort]?, range: (lower: Int, upper: Int), on conn: DatabaseConnectable) throws -> EventLoopFuture<([Company], Int)>
    func attach(company: Company, employee: Employee, on conn: DatabaseConnectable) throws -> Future<CompanyEmployee>
}

final class SQLCompanyEmployeeRepository: CompanyEmployeeRepository {
    func findEmployees(by company: Company, criteria: [FilterOperator<Employee.Database, Employee>], sortBy: [Employee.Database.QuerySort]?, range: (lower: Int, upper: Int), on conn: DatabaseConnectable) throws -> Future<([Employee], Int)> {
        
        let queryBuilder = try company.Employees.query(on: conn)
        
        return queryBuilder.count().flatMap { (total) in
            return queryBuilder
                .range(lower: range.lower, upper: range.upper)
                .sort(by: sortBy)
                .all()
                .map { (employees)  in
                    return (employees, total)
            }
        }
    }
    
    func findCompany(by employee: Employee, criteria: [FilterOperator<Company.Database, Company>], sortBy: [Company.Database.QuerySort]?, range: (lower: Int, upper: Int), on conn: DatabaseConnectable) throws -> EventLoopFuture<([Company], Int)> {
        let queryBuilder = try employee.companies.query(on: conn)
        
        for filter in criteria {
            queryBuilder.filter(filter)
        }
        
        return queryBuilder.count().flatMap { (total) in
            queryBuilder
                .sort(by: sortBy)
                .all()
                .map { (companies) in
                    return (companies, total)
            }
        }
    }
    
    
    func attach(company: Company, employee: Employee, on conn: DatabaseConnectable) throws -> EventLoopFuture<CompanyEmployee> {
        let companyEmployee = try CompanyEmployee.init(company, employee)
        return companyEmployee.save(on: conn)
    }
    
    
}
