import Vapor

struct CompanyListResponse: Content {
    var companies: [Company]
    var lower: Int
    var upper: Int
    var total: Int
}
