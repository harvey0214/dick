import Vapor

struct EmployeeListResponse: Content {
    var Empolyees: [Employee]
    var lower: Int
    var upper: Int
    var total: Int
}

extension EmployeeListResponse {
    static func create(employees: [Employee], lower: Int, upper: Int, total: Int) -> EmployeeListResponse {
        return EmployeeListResponse(Empolyees: employees, lower: lower, upper: upper, total: total)
    }
}
