import Vapor

struct ClassDetailResponse: Content {
    var className: String?
    var peopleCount: Int?
    var grade: Float?
}

extension ClassDetailResponse {
    public static func createDetailResponse(with classInfo: Classroom) -> ClassDetailResponse {
        return ClassDetailResponse(className: classInfo.className ?? "", peopleCount: classInfo.peopleCount ?? 0, grade: classInfo.grade ?? 0)
    }
}
