import Vapor

struct UserInfoListResponse: Content {
    var userInfo: [UserInfo]
    var lower: Int
    var upper: Int
    var total: Int
}

extension UserInfoListResponse {
    static func create(userInfos: [UserInfo], range: (lower: Int, upper: Int), total: Int) -> UserInfoListResponse {
        return UserInfoListResponse(userInfo: userInfos, lower: range.lower, upper: range.upper, total: total)
    }
}
