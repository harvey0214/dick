import Vapor

struct UserInfoResponse: Content {
    let name: String
    let age: Int
    let gender: String
}


extension UserInfoResponse {
    public static func createDetailRespone(wit userInfo: UserInfo) -> UserInfoResponse {
        
        return UserInfoResponse(name: userInfo.name!, age: userInfo.age!, gender: userInfo.gender!)
    }
}
