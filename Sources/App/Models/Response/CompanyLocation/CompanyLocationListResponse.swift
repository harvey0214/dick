import Vapor

struct CompanyLocationListResponse: Content {
    var lower: Int
    var upper: Int
    var total: Int
    var locations: [CompanyLocation]
}
