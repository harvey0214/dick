import Vapor

struct QueryCompanyLocationList: Content {
    var lower: Int?
    var upper: Int?
}
