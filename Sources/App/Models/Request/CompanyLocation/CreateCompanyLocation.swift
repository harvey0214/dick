import Vapor

struct CreateCompanyLocation: Content {
    var location: String
}
