import Vapor

struct CreateClassRequest: Content {
    var className: String
    var peopleCount: Int
    var grade: Float
    var teacher: String
}

extension Classroom {
    convenience init(from payload: CreateClassRequest) {
        self.init(className: payload.className, peopleCount: payload.peopleCount, grade: payload.grade, teacher: payload.teacher)
    }
}
