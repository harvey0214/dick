import Vapor

struct CreateEmployeeRequest: Content {
    var name: String
    var age: Int
    var workYear: Int
}

extension Employee {
    convenience init(payload: CreateEmployeeRequest) {
        self.init(name: payload.name, age: payload.age, workYear: payload.workYear)
    }
}

