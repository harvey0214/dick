import Vapor

struct QueryEmployeeListRequest: Content {
    var lower: Int?
    var upper: Int?
    var keyword: String?
    var age: Int?
}
