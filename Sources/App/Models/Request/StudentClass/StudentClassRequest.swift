import Vapor

struct StudentClassRequest: Decodable {
    var studentIndex: Int
    var classIndex: Int
}
