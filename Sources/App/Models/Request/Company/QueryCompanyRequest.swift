import Vapor

struct QueryCompanyRequest: Content {
    var lower: Int?
    var upper: Int?
    var age: Int?
    var majorOfProduct: String?
}
