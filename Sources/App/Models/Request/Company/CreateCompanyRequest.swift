import Vapor

struct CreateCompanyRequest: Content {
    var companyName: String
    var age: Int
    var majorOfProduct: String
}


extension Company {
    convenience init(payload: CreateCompanyRequest) {
        self.init(companyName: payload.companyName, age: payload.age, majorOfProduct: payload.majorOfProduct)
    }
}
