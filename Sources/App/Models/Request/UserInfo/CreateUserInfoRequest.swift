import Vapor

struct CreateUserInfoRequest: Content {
    var name: String
    var age: Int
    var gender: String
    var classroom_id: Int
}

extension UserInfo {
    convenience init(from payload: CreateUserInfoRequest) {
        self.init(name: payload.name, age: payload.age, gender: payload.gender, classroom_id: payload.classroom_id)
    }
}
