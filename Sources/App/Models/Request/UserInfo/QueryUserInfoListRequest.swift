import Vapor

struct QueryUserInfoListRequest: Content {
    let gender: String?
    let age: Int?
    let lower: Int?
    let upper: Int?
}
