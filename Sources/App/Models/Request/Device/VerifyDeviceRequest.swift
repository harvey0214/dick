import Vapor

struct VerifyDeviceRequest: Content {
    var token: String
    var catalog: String
}

extension Device {
    convenience init(payload: VerifyDeviceRequest) {
        self.init(token: payload.token, catalog: payload.catalog)
    }
}
