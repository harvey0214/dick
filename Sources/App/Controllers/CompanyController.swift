import VaporExt
import Authentication


final class CompanyController: RouteCollection {
    func boot(router: Router) throws {
        let company = router.grouped("company")
        company.get(use: index)
        company.get("all", use: fetchAll)
        company.get(Company.parameter, use: fetchOne)
        company.post(CreateCompanyRequest.self, use: create)
        company.get(Company.parameter, "loactions", use: fetchLocations)
    }
    
    func create(_ req: Request, payload: CreateCompanyRequest) throws -> Future<HTTPStatus> {
        return try req.make(CompanyRepository.self).create(Company.init(payload: payload), on: req).flatMap({ (company)  in
            let _ = try req.authenticatedSession(Company.self)
            return req.future(.ok)
        })
    }
    
    func fetchAll(_ req: Request) throws -> Future<[Company]> {
        return try req.make(CompanyRepository.self).fetchAll(on: req)
    }
    
    func fetchOne(_ req: Request) throws -> Future<Company> {
        return try req.parameters.next(Company.self)
    }
    
    func index(_ req: Request) throws -> Future<CompanyListResponse> {
        var criteria: [FilterOperator<Company.Database, Company>] = []
        let sortBy: [Company.Database.QuerySort] = []
        let form = try req.query.decode(QueryCompanyRequest.self)
        
        
        if let age = form.age {
            criteria.append(\Company.age == age)
        }
        
        if let product = form.majorOfProduct {
            criteria.append(\Company.majorOfProduct == product)
        }
        
        
        
        
        return try req.make(CompanyRepository.self).find(by: criteria, sortBy: sortBy, range: (form.lower ?? 0 , form.upper ?? 0), keyword: "", on: req, withSoftDelete: false).map({ (companies, total) in
            return CompanyListResponse(companies: companies, lower: form.lower ?? 0, upper: form.upper ?? 0, total: total)
        })
        
    }
    
    func fetchLocations(_ req: Request) throws -> Future<CompanyLocationListResponse> {
        return try req.parameters.next(Company.self).flatMap({ (company) in
            let criteria: [FilterOperator<CompanyLocation.Database, CompanyLocation>] = []
            let sortBy: [CompanyLocation.Database.QuerySort] = []
            let form = try req.query.decode(QueryCompanyLocationList.self)
            
            
            return try req.make(CompanyRepository.self).findLocations(company, by: criteria, sortBy: sortBy, range: (form.lower ?? 0, form.upper ?? 0), on: req, withSoftDeleted: false).map({ (locations, total) in
                return CompanyLocationListResponse(lower: form.lower ?? 0, upper: form.upper ?? 0, total: total, locations: locations)
            })
        })
    }
}
