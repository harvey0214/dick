
import VaporExt
import Authentication

final class EmployeeController: RouteCollection {
    func boot(router: Router) throws {
        let employee = router.grouped("employee")
        
//        employee.get(use: index)
        employee.get("all", use: fetchAll)
        employee.get(Employee.parameter, use: fetchOne)
        employee.post(CreateEmployeeRequest.self, use: create)
        employee.grouped(Employee.authSessionsMiddleware()).get(use: index)
    }
    
    func fetchAll(_ req: Request) throws -> Future<EmployeeListResponse> {
        return try req.make(EmployeeRepository.self).fetchAll(on: req).map({ (employees) in
            return EmployeeListResponse(Empolyees: employees, lower: 0, upper: employees.count, total: employees.count)
        })
    }
    
    func fetchOne(_ req: Request) throws -> Future<Employee> {
        return try req.parameters.next(Employee.self)
    }
    
    func create(_ req: Request, payload: CreateEmployeeRequest) throws -> Future<Employee> {
        return try req.make(EmployeeRepository.self).create(Employee.init(payload: payload), on: req).map({ (employee) in
            try req.authenticateSession(employee)
            return employee
        })
    }
    
    func index(_ req: Request) throws -> Future<EmployeeListResponse> {
        var criteria: [FilterOperator<Employee.Database, Employee>] = []
        let form = try req.query.decode(QueryEmployeeListRequest.self)
        let sort: [Employee.Database.QuerySort]? = []
        
        if let age = form.age {
            criteria.append(\Employee.age == age)
        }
        
        return try req.make(EmployeeRepository.self).find(by: criteria, sortBy: sort, range: (lower: form.lower ?? 0, upper: form.upper ?? 0), keyword: form.keyword, on: req, withSoftDelete: false).map({ (employees, total) in
            return EmployeeListResponse(Empolyees: employees, lower: form.lower ?? 0, upper: form.upper ?? 0, total: total)
        })
    }
    

    
    
}
