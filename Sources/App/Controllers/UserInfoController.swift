import VaporExt


final class UserInfoController: RouteCollection {
    
    func boot(router: Router) throws {
        let userInfo = router.grouped("UserInfo")
        
//        userInfo.get(use: findAll)
        userInfo.post(CreateUserInfoRequest.self, use: create)
        userInfo.get(use: findAll)
        userInfo.get("class", UserInfo.parameter, use: getUserClasse)
        userInfo.get(UserInfo.parameter, use: findOne)
    }
    
    func findAll(_ req: Request) throws -> Future<[UserInfo]> {
        
        let repository = try req.make(UserInfoRepository.self)
        return repository.fetchAll(on: req)
    }
    
    func create(_ req: Request, payload: CreateUserInfoRequest) throws -> Future<UserInfo> {
        let userInfo = UserInfo(from: payload)
        let repository = try req.make(UserInfoRepository.self)
        return repository.create(userInfo, on: req)
    }
    
    func index(_ req: Request) throws -> Future<UserInfoListResponse> {
        let criteria: [FilterOperator<UserInfo.Database, UserInfo>] = []
        let sort: [UserInfo.Database.QuerySort] = []
        let form = try req.query.decode(QueryUserInfoListRequest.self)
        
        let repository = try req.make(UserInfoRepository.self)
        return repository.find(by: criteria, sortBy: sort, range: (lower: form.lower!, upper: form.upper!), gender: form.gender!, age: form.age!, on: req, withSoftDeleted: false).map { (userInfo, total) in
            return UserInfoListResponse(userInfo: userInfo, lower: form.lower!, upper: form.upper!, total: total)
        }
    }
    
    func getUserClasse(_ req: Request) throws -> Future<[Classroom]> {
        return try req.parameters.next(UserInfo.self).flatMap { (userInfo) -> EventLoopFuture<[Classroom]> in
            return userInfo.inClass.query(on: req).all()
        }
    }
    func findOne(_ req: Request) throws -> Future<UserInfo> {
        return try req.parameters.next(UserInfo.self)
    }
}
