import VaporExt
import Authentication

final class ClassController: RouteCollection {
    func boot(router: Router) throws {
        let classPath = router.grouped("Classes")
        
        classPath.grouped(Classroom.authSessionsMiddleware()).get(use: findAll)
//        classPath.get(use: findAll)
        classPath.post(CreateClassRequest.self, use: create)
        classPath.get(Classroom.parameter, "UserInfos", use: findUserInfos)
        
    }
    
    func findAll(_ req: Request) throws -> Future<[Classroom]> {
        let repository = try req.make(ClassroomRepository.self)
        print(try req.session().data)
        return repository.findAll(on: req)
        
    }
    
    func create(_ req: Request, classRequest: CreateClassRequest) throws -> EventLoopFuture<Classroom> {
        let repository = try req.make(ClassroomRepository.self)
        let classInfo = Classroom(from: classRequest)
        return repository.create(classInfo, on: req)
    }
    
    func findClassUserInfos(_ req: Request) throws -> Future<[UserInfo]> {
        let criteria: [FilterOperator<Classroom.Database, Classroom>] = []
        let sort: [Classroom.Database.QuerySort] = []
        let form = try req.query.decode(QueryClassUserInfos.self)
        
        let repository = try req.make(ClassroomRepository.self)
        return repository.findClassroomUserInfos(by: criteria, sortBy: sort, index: form.index, on: req, withSoftDeleted: false)
    }
    
    func findUserInfos(_ req: Request) throws -> Future<[UserInfo]> {
        return try req.parameters.next(Classroom.self).flatMap { (classroom) -> EventLoopFuture<[UserInfo]> in
            return try classroom.userInfos.query(on: req).all()
        }
    }
    
    //    func cookie(_ req: Request) throws -> Future<HTTPStatus> {
    //        return re
    //    }
    
}
