import VaporExt

final class StudentClassController: RouteCollection {
    func boot(router: Router) throws {
        let pivotPath = router.grouped("StudentClassroom")
        pivotPath.post(UserInfo.parameter, "classroom", Classroom.parameter, use: create)
        pivotPath.get(use: findAll)
        pivotPath.get(StudentClass.parameter, use: findOne)
        pivotPath.get("class", Classroom.parameter, "UserInfos", use: findStudents)
        pivotPath.get("userInfo", UserInfo.parameter, "classes", use: findClasses)
    }
    
    func create(_ req: Request) throws -> Future<StudentClass> {
        return try req.parameters.next(UserInfo.self).flatMap({ (userInfo) -> Future<StudentClass> in
            return try req.parameters.next(Classroom.self).flatMap({ (classroom) -> Future<StudentClass> in
                
                guard let userInfo_id = userInfo.id, let classroom_id = classroom.id else { throw Abort(.badRequest)}
                
                let criteria: [FilterOperator<StudentClass.Database, StudentClass>] = [
                    \StudentClass.student_id == userInfo_id,
                    \StudentClass.class_id == classroom_id
                ]
                
                return try req.make(StudentClassRepository.self).findOne(by: criteria, sortBy: [], on: req, withSoftDeleted: false).flatMap({ (studentClass) -> Future<StudentClass> in
                    
                    guard let alreadyExist = studentClass else {
                        return try req.make(StudentClassRepository.self).attach(classroom: classroom, student: userInfo, on: req)
                    }
                    return req.future(alreadyExist)
                }).always {
                    print("love tototototo")
                }
                
            })
        })
    }
    
    func findOne(_ req: Request) throws -> Future<StudentClass> {
        return try req.parameters.next(StudentClass.self)
    }
    
    func findStudents(_ req: Request) throws -> Future<[UserInfo]> {
        return try req.parameters.next(Classroom.self).flatMap({ (classroom) -> Future<[UserInfo]> in
            guard let classroom_id = classroom.id else {throw Abort(.badRequest)}
            let criteria: [FilterOperator<StudentClass.Database, StudentClass>] = [\StudentClass.class_id == classroom_id]
            let repository = try req.make(StudentClassRepository.self)
            return try repository.findUserInfos(by: classroom, criteria: criteria, sortBy: [], on: req, withSoftDeleted: false)
        })
    }
    
    func findAll(_ req: Request) -> Future<[StudentClass]> {
        return StudentClass.query(on: req).all()
    }
    
    func findClasses(_ req: Request) throws -> Future<[Classroom]> {
        return try req.parameters.next(UserInfo.self).flatMap({ (student) -> EventLoopFuture<[Classroom]> in
            return try req.make(StudentClassRepository.self).findClassroom(by: student, criteria: [], sortBy: [], on: req, withSoftDeleted: false)
        })
    }

    
}
