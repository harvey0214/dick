import VaporExt

final class CompanyEmployeeController: RouteCollection {
    
    func boot(router: Router) throws {
        let company = router.grouped("company")
        let employee = router.grouped("employee")
        
        employee.get(Employee.parameter, "companies", use: relatedCompanies)
        employee.post(Employee.parameter, "companies", Company.parameter, use: create)
        
        company.get(Company.parameter, "employees", use: relatedEmployee)
        company.post(Company.parameter, "employees", Employee.parameter, use: create)
        
    }
    
    func relatedCompanies(_ req: Request) throws -> Future<CompanyListResponse> {
        let form = try req.query.decode(QueryCompanyRequest.self)
        var criteria: [FilterOperator<Company.Database, Company>] = []
        let sortBy: [Company.Database.QuerySort] = []
        
        
        if let age = form.age {
            criteria.append(\Company.age == age)
        }
        
        if let major = form.majorOfProduct {
            criteria.append(\Company.majorOfProduct == major)
        }
        
        return try req.parameters.next(Employee.self).flatMap({ (employee) in
            return try req.make(CompanyEmployeeRepository.self).findCompany(by: employee, criteria: criteria, sortBy: sortBy, range: (form.lower ?? 0, form.upper ?? 0), on: req).map({ (companies, total) in
                return CompanyListResponse(companies: companies, lower: form.lower ?? 0 , upper: form.upper ?? 0, total: total)
            })
        })
        
    }
    
    func relatedEmployee(_ req: Request) throws -> Future<EmployeeListResponse> {
        let form = try req.query.decode(QueryEmployeeListRequest.self)
        var criteria: [FilterOperator<Employee.Database, Employee>] = []
        let sortBy: [Employee.Database.QuerySort]? = []
        
        
        if let age = form.age {
            criteria.append(\Employee.age == age)
        }
        
        
        
        return try req.parameters.next(Company.self).flatMap({ (company) in
            return try req.make(CompanyEmployeeRepository.self).findEmployees(by: company, criteria: criteria, sortBy: sortBy, range: (lower: form.lower ?? 0, upper: form.upper ?? 0), on: req).map({ (employees, total) in
                return EmployeeListResponse(Empolyees: employees, lower: form.lower ?? 0, upper: form.upper ?? 0, total: total)
            })
        })
    }
    
    func create(_ req: Request) throws -> Future<CompanyEmployee> {
        return try req.parameters.next(Company.self)
            .flatMap { (company) in
                try req.parameters.next(Employee.self)
                    .flatMap({ (employee) in
                        try req.make(CompanyEmployeeRepository.self)
                            .attach(company: company, employee: employee, on: req)
                    })
        }
    }
    
    
    
}
