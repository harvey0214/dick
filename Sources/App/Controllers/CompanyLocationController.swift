import Vapor

final class CompanyLocationController: RouteCollection {
    func boot(router: Router) throws {
        let company = router.grouped("company")
        company.post(CreateCompanyLocation.self, at: Company.parameter, use: create)
    }
    
    func create(_ req: Request, payload: CreateCompanyLocation) throws -> Future<CompanyLocation> {
        return try req.parameters.next(Company.self).flatMap { (company) in
            let location = CompanyLocation(company_id: company.id ?? 0, location: payload.location)
            return try req.make(CompanyLocationRepository.self).create(location, on: req)
        }
    }
    
    
}
