import FluentPostgreSQL
import Vapor
import Authentication


/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    
    Environment.dotenv() 
    // Register providers first
    try services.register(FluentPostgreSQLProvider())
     try services.register(AuthenticationProvider())
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    // Register middleware
    var middlewaresConfig = MiddlewareConfig()
    try middlewares(config: &middlewaresConfig)
    
    // Catches errors and converts to HTTP response
    services.register(middlewaresConfig)

    // Configure a Postgres database
    var databases = DatabasesConfig()
    
    
    let databaseConfig = PostgreSQLDatabaseConfig(hostname: Environment.get(AppEnvironment.PSQL_HOSTNAME.value) ?? "localhost", port: Environment.get(AppEnvironment.PSQL_PORT.value) ?? 5432, username: Environment.get(AppEnvironment.PSQL_USERNAME.value) ?? "username", database: Environment.get(AppEnvironment.PSQL_DATABASE_NAME.value) ?? "dbName", password: Environment.get(AppEnvironment.PSQL_PASSWORD.value) ?? "1234", transport: .cleartext)
    databases.enableLogging(on: .psql)
    let database = PostgreSQLDatabase(config: databaseConfig)
    databases.add(database: database, as: .psql)
    services.register(databases)
  
    // Configure migrations
    var migrations = MigrationConfig()
      migrate(config: &migrations)
    services.register(migrations)
  
    
    // revert
    var commandConfig = CommandConfig.default()
    commandConfig.useFluentCommands()
    services.register(commandConfig)
    
    
    // Register FluentPostgreSQL
    // try services.register(FluentSQLiteProvider())
    
    // addrepostory
    setupRepositories(services: &services)
    
    // add Session
    try setupCacheSessions(services: &services)
    config.prefer(DatabaseKeyedCache<ConfiguredDatabase<PostgreSQLDatabase>>.self, for: KeyedCache.self)
    
    
   
}
