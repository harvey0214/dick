enum AppEnvironment: String {
    case PSQL_HOSTNAME
    case PSQL_PORT
    case PSQL_USERNAME
    case PSQL_DATABASE_NAME
    case PSQL_PASSWORD
    case PSQL_LOGS
    
    case COOKIE_SECURE
    case COOKIE_DOMAIN
    
    
    var value: String {
        get {
            return self.rawValue
        }
    }
}
