import Vapor

public func setupRepositories(services: inout Services) {
    services.register(SQLUserInfoRepository(), as: UserInfoRepository.self)
    services.register(SQLClassRepository(), as: ClassroomRepository.self)
    services.register(SQLStudenetClassRepository(), as: StudentClassRepository.self)
    services.register(SQLEmployeeRepository(), as: EmployeeRepository.self)
    services.register(SQLCompanyRepository(), as: CompanyRepository.self)
    services.register(SQLCompanyEmployeeRepository(), as: CompanyEmployeeRepository.self)
    services.register(SQLCompanyLocationRepository(), as: CompanyLocationRepository.self)
}	
